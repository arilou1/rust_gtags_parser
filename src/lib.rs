use ra_ap_ide::Analysis;
use ra_ap_syntax::{AstNode, NodeOrToken, SyntaxKind, TextSize, WalkEvent};
use std::collections::HashSet;
use std::ffi::{c_char, c_void, CStr, CString};
use std::fs;

// tag type
const PARSER_DEF: i32 = 1;
const PARSER_REF_SYM: i32 = 2;

type ParserCallback = extern "C" fn(
    tag_type: i32,
    tag_name: *const c_char,
    line_number: i32,
    file_path: *const c_char,
    signature: *const c_char,
    user_data: *mut c_void,
);

#[repr(C)]
pub struct ParserParam {
    size: i32, // size of this structure
    flags: i32,
    file: *const c_char,
    put: ParserCallback,
    arg: *mut c_void,
    isnotfunction: extern "C" fn(*const c_char) -> i32,
    langmap: *const c_char,
    getconf: extern "C" fn(*const c_char) -> *mut c_char,
    die: extern "C" fn(*const c_char, ...),
    warning: extern "C" fn(*const c_char, ...),
    message: extern "C" fn(*const c_char, ...),
}

struct TextLines {
    newlines: Vec<usize>,
    lines: Vec<String>,
}

impl TextLines {
    fn new(text: &str) -> Self {
        let mut newlines = Vec::new();
        let mut lines = Vec::new();
        let mut start = 0;

        for (index, character) in text.char_indices() {
            if character == '\n' {
                newlines.push(index);
                // Capture the line, excluding the newline character
                lines.push(text[start..index].to_string());
                start = index + 1; // Start of the next line
            }
        }

        // Don't forget the last line if the text doesn't end with a newline
        if start < text.len() {
            lines.push(text[start..].to_string());
        }

        Self { newlines, lines }
    }

    fn line_info(&self, offset: usize) -> Option<(usize, &str)> {
        match self.newlines.binary_search(&offset) {
            Ok(line) => Some((line + 1, &self.lines[line])),
            Err(line) => {
                if line == 0 {
                    Some((1, &self.lines[0]))
                } else if line < self.lines.len() {
                    Some((line + 1, &self.lines[line]))
                } else {
                    None
                }
            }
        }
    }
}

#[no_mangle]
pub extern "C" fn parser(param: *const ParserParam) {
    let param = unsafe { &*param };

    // Read the file
    let file_data = {
        let file = unsafe { CStr::from_ptr(param.file) };
        let Ok(path) = file.to_str() else {
            return;
        };
        fs::read_to_string(path)
    };
    let Ok(file_data) = file_data else {
        return;
    };

    // Parse into TextLines
    let text_lines = TextLines::new(&file_data);

    // Analyze the file
    let (analysis, file_id) = Analysis::from_single_file(file_data);
    let Ok(parsed) = analysis.parse(file_id) else {
        return;
    };

    let mut symbols_locs: HashSet<TextSize> = HashSet::new();
    // Loads all the symbols definitions
    let structure = analysis.file_structure(file_id).unwrap();
    for s in structure {
        symbols_locs.insert(s.navigation_range.start());
    }

    // Load all references
    let root_node = parsed.syntax();
    for event in root_node.preorder_with_tokens() {
        if let WalkEvent::Enter(element) = event {
            if let NodeOrToken::Token(token) = &element {
                if let SyntaxKind::IDENT = token.kind() {
                    let tag_name = CString::new(token.text()).unwrap();
                    let line = text_lines
                        .line_info(token.text_range().start().into())
                        .unwrap();
                    let signature = CString::new(line.1).unwrap();
                    (param.put)(
                        match symbols_locs.contains(&element.text_range().start()) {
                            true => PARSER_DEF,
                            false => PARSER_REF_SYM,
                        },
                        tag_name.as_ptr(),
                        line.0 as i32,
                        param.file,
                        signature.as_ptr(),
                        param.arg,
                    );
                }
            }
        }
    }
}
