# rust_gtags_parser


## Building instructions
```
cargo build --release
```

## Install instructions

Add to your .globalrc the new parser

```
# Custom parser for .rs files
rust-parser|Rust language custom parser:\
    :tc=common:\
    :langmap=Rust\:.rs:\
    :gtags_parser=Rust\:/path/to/librust_gtags_parser.so:
```

Now you can add it to your default parser in .globalrc for example:
```
#
# Plug-in parser to use Pygments.
#
pygments-parser|Pygments plug-in parser:\
	:tc=common:\
	:tc=rust-parser:\
	:ctagscom=/univ_ctags/bin/ctags:\
	:pygmentslib=$libdir/gtags/pygments-parser.la:\
    ...
```
